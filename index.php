<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new animal("shaun");
echo "Nama : ".$sheep->name . "<br>";
echo "Legs : ".$sheep->legs . "<br>";
echo "cold blooded : ".$sheep->cold_blooded . "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama : ".$kodok->name . "<br>";
echo "Legs : ".$kodok->legs . "<br>";
echo "cold blooded : ".$kodok->cold_blooded . "<br>";
echo $kodok->Jump();
echo "<br>";
echo "<br>";


$sungokong  = new Ape("kera sakti");
echo "Nama : ".$sungokong ->name . "<br>";
echo "Legs : ".$sungokong ->legs . "<br>";
echo "cold blooded : ".$sungokong ->cold_blooded . "<br>";
echo $sungokong->yell();     